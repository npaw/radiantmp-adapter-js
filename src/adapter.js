var youbora = require('youboralib')
var manifest = require('../manifest.json')

youbora.adapters.RadiantMP = youbora.Adapter.extend({
  /** Override to return current plugin version */
  getVersion: function () {
    return manifest.version + '-' + manifest.name + '-' + manifest.tech
  },

  /** Override to return current playhead of the video */
  getPlayhead: function () {
    return this.player.getCurrentTime() / 1000
  },

  /** Override to return current playrate */
  getPlayrate: function () {
    return this.player.getPlaybackRate()
  },

  /** Override to return video duration */
  getDuration: function () {
    return this.player.getDuration() / 1000
  },

  /** Override to return current bitrate */
  getBitrate: function () {
    var renditions = this.player.getBitrates()
    if (renditions) {
      for (var rendition in renditions) {
        if (renditions[rendition].active) {
          var rendObject = renditions[rendition]
          if (rendObject.bitrate) return rendObject.bitrate
          if (rendObject.bandwidth) return rendObject.bandwidth
          return null
        }
      }
    }
    return null
  },

  /** Override to return user bandwidth throughput */
  getThroughput: function () {
    return rmp.getHlsBwEstimate()
  },

  /** Override to return rendition */
  getRendition: function () {
    var renditions = this.player.getBitrates()
    if (renditions) {
      for (var rendition in renditions) {
        if (renditions[rendition].active) {
          return youbora.Util.buildRenditionString(renditions[rendition].width, renditions[rendition].height, renditions[rendition].bitrate || renditions[rendition].bandwidth || null)
        }
      }
    }
    return null
  },

  /** Override to return title */
  getTitle: function () {
    return this.player.getContentTitle() || 'unknown'
  },

  /** Override to recurn true if live and false if VOD */
  getIsLive: function () {
    return this.player.isLive
  },

  /** Override to return resource URL. */
  getResource: function () {
    return this.player.getSrc()
  },

  /** Override to return player version */
  getPlayerVersion: function () {
    return this.player.getPlayerVersion()
  },

  /** Override to return player's name */
  getPlayerName: function () {
    return 'RadiantMP-' + this.player.getPlayerMode()
  },

  /** Register listeners to this.player. */
  registerListeners: function () {
    // Console all events if logLevel=DEBUG
    youbora.Util.logAllEvents(this.player.dom.container)

    // References
    this.references = []
    this.references['ready'] = this.registerAdsListener.bind(this)
    this.references['play'] = this.playListener.bind(this)
    this.references['pause'] = this.pauseListener.bind(this)
    this.references['playing'] = this.playingListener.bind(this)
    this.references['error'] = this.errorListener.bind(this)
    this.references['seeking'] = this.seekingListener.bind(this)
    this.references['seeked'] = this.seekedListener.bind(this)
    this.references['bufferstalled'] = this.bufferingListener.bind(this)
    this.references['buffernotstalledanymore'] = this.bufferedListener.bind(this)
    this.references['ended'] = this.endedListener.bind(this)
    this.references['timeupdate'] = this.timeupdateListener.bind(this)
    this.references['loadstart'] = this.playListener.bind(this)

    // Register listeners
    for (var key in this.references) {
      this.player.dom.container.addEventListener(key, this.references[key])
    }
  },

  /** Unregister listeners to this.player. */
  unregisterListeners: function () {
    // Disable playhead monitoring

    // unregister listeners
    if (this.player && this.references) {
      for (var key in this.references) {
        this.player.dom.container.removeEventListener(key, this.references[key])
      }
      this.references = []
    }
  },

  registerAdsListener: function (e) {
    this.plugin.setAdsAdapter(new youbora.adapters.RadiantMP.NativeAdsAdapter(this.player))
  },

  /** Listener for 'play' event. */
  playListener: function (e) {
    this.fireStart()
  },

  /** Listener for 'timeupdate' event. */
  timeupdateListener: function (e) {
    if (this.getPlayhead() > 0.1) {
      this.fireJoin()
    }
  },

  /** Listener for 'pause' event. */
  pauseListener: function (e) {
    this.firePause()
  },

  /** Listener for 'playing' event. */
  playingListener: function (e) {
    this.fireResume()
    this.fireSeekEnd()
    this.fireBufferEnd()
    this.fireJoin()
  },

  /** Listener for 'error' event. */
  errorListener: function (e) {
    var code = null
    var message = null
    var fatal = false
    if (this.player.errorData) {
      code = this.player.errorData.code
      message = this.player.errorData.message
      fatal = this.player.errorData.fatal
    }
    this.fireError(code, message)
    if (fatal) {
      this.plugin.fireStop()
    }
  },

  /** Listener for 'seeking' event. */
  seekingListener: function (e) {
    this.fireSeekBegin()
  },

  /** Listener for 'seeked' event. */
  seekedListener: function (e) {
    this.fireSeekEnd()
  },

  /** Listener for 'buffering' event. */
  bufferingListener: function (e) {
    this.fireBufferBegin()
  },

  /** Listener for 'buffered' event. */
  bufferedListener: function (e) {
    this.fireBufferEnd()
  },

  /** Listener for 'ended' event. */
  endedListener: function (e) {
    if (!this.blockedStop) {
      this.fireStop()
    }
  }
}, {
  NativeAdsAdapter: require('./ads/native')
})

module.exports = youbora.adapters.RadiantMP
