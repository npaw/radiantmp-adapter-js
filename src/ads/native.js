var youbora = require('youboralib')
var manifest = require('../../manifest.json')

var GenericAdsAdapter = youbora.Adapter.extend({
  getVersion: function () {
    return manifest.version + '-radiantmp-ads'
  },

  getDuration: function () {
    return this.player.getAdDuration() / 1000
  },

  getResource: function () {
    return this.player.getAdMediaUrl()
  },

  getPlayhead: function () {
    return this.player.getAdCurrentTime() / 1000
  },

  getTitle: function () {
    return this.player.getAdTitle()
  },

  getPosition: function () {
    var playhead = this.plugin.getAdapter().getPlayhead()
    var duration = this.plugin.getAdapter().getDuration()
    if (playhead < 0.1) {
      return youbora.Constants.AdPosition.Preroll
    }
    if (playhead === duration && duration !== 0) {
      return youbora.Constants.AdPosition.Postroll
    }
    return youbora.Constants.AdPosition.Midroll
  },

  getGivenAds: function () {
    return this.player.getAdPodInfo() ? this.player.getAdPodInfo().getTotalAds() : null
  },

  getCreativeId: function () {
    return this.player.getAdCreativeId()
  },

  getIsVisible: function () {
    if (this.player.dom.adContainer) {
      return youbora.Util.calculateAdViewability(this.player.dom.adContainer)
    }
    return true
  },

  getIsFullscreen: function () {
    return this.player.getFullscreen()
  },

  getAudioEnabled: function () {
    return !this.player.getMute()
  },

  getGivenBreaks: function () {
    var ret = 0
    if (this.player.adsManager) {
      ret = this.player.adsManager.getCuePoints().length
    }
    return ret
  },

  getBreaksTime: function () {
    if (!this.player.adsManager) {
      return null
    }
    var cuepoints = this.player.adsManager.getCuePoints()
    cuepoints.forEach(function (value, index) {
      if (cuepoints[index] === -1 || cuepoints[index] === null || cuepoints[index] === undefined) {
        cuepoints[index] = this.plugin._adapter.getDuration()
      }
    }.bind(this))
    return cuepoints
  },

  getIsSkippable: function () {
    if (typeof this.player.getAdSkippableState !== 'undefined') {
      return this.player.getAdSkippableState()
    }
    return this.player.adSkipButton
  },

  /** Register listeners to this.player. */
  registerListeners: function () {
    // Enable playhead monitor (buffer = true, seek = false)
    this.monitorPlayhead(true, false)

    // References
    this.references = {}
    this.references['adtagstartloading'] = this.playListener.bind(this)
    this.references['adstarted'] = this.joinListener.bind(this)
    this.references['adpaused'] = this.pauseListener.bind(this)
    this.references['adresumed'] = this.resumeListener.bind(this)
    this.references['aderror'] = this.errorListener.bind(this)
    this.references['adclosed'] = this.endedListener.bind(this)
    this.references['adcomplete'] = this.endedListener.bind(this)
    this.references['adskipped'] = this.skippedListener.bind(this)
    this.references['adclick'] = this.onClickListener.bind(this)
    this.references['adloadererror'] = this.notServedListener.bind(this)
    this.references['adblockerdetected'] = this.blockedListener.bind(this)
    this.references['adalladscompleted'] = this.endedAdsListener.bind(this)
    this.references['adfirstquartile'] = this.firstQuartileListener.bind(this)
    this.references['admidpoint'] = this.secondQuartileListener.bind(this)
    this.references['adthirdquartile'] = this.thirdQuartileListener.bind(this)
    // Register listeners
    for (var key in this.references) {
      this.player.dom.container.addEventListener(key, this.references[key])
    }
  },

  /** Unregister listeners to this.player. */
  unregisterListeners: function () {
    // Disable playhead monitoring
    if (this.monitor) this.monitor.stop()

    // unregister listeners
    if (this.player && this.references) {
      for (var key in this.references) {
        this.player.dom.container.removeEventListener(key, this.references[key])
      }
      this.references = []
    }
  },

  /** Listener for 'adtagstartloading' event. */
  playListener: function (e) {
    if (this.player.adParser !== 'rmp-vast') this.plugin.getAdapter().blockedStop = true
    // this.fireStart()
  },

  /** Listener for 'adstarted' event. */
  joinListener: function (e) {
    if (this.player.adParser !== 'rmp-vast') this.plugin.getAdapter().blockedStop = true
    this.fireStart()
    this.fireJoin()
    this.duration = this.getDuration()
  },

  /** Listener for 'adpaused' event. */
  pauseListener: function (e) {
    this.firePause()
  },

  /** Listener for 'adresumed' event. */
  resumeListener: function (e) {
    this.fireResume()
    this.fireJoin()
  },

  /** Listener for 'aderror' event. */
  errorListener: function (e) {
    this.fireError()
  },

  /** Listener for 'adclosed' and 'adcomplete' events. */
  endedListener: function (e) {
    this.fireStop({ 'adPlayhead': this.duration })
  },

  /** Listener for 'adskipped' event. */
  skippedListener: function (e) {
    this.fireSkip()
  },

  /** Listener for 'adclick' event. */
  onClickListener: function (e) {
    this.fireClick()
  },

  /** Listener for 'adloadererror' event. */
  notServedListener: function (e) {
    this.fireError('Ad not served')
    this.fireStop()
  },

  /** Listener for 'adblockerdetected' event. */
  blockedListener: function (e) {
    this.fireError('Ad blocked')
    this.fireStop()
  },

  /** Listener for 'adalladscompleted' event. */
  endedAdsListener: function (e) {
    this.plugin.getAdapter().blockedStop = false
    this.fireStop()
    if (this.getPosition() === youbora.Constants.AdPosition.Postroll) {
      this.plugin.getAdapter().fireStop()
    }
  },

  firstQuartileListener: function (e) {
    this.fireQuartile(1)
  },

  secondQuartileListener: function (e) {
    this.fireQuartile(2)
  },

  thirdQuartileListener: function (e) {
    this.fireQuartile(3)
  }

})

module.exports = GenericAdsAdapter
